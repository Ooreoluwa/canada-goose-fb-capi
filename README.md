### Canada Goose

Consists of a cloud function handling custom purchase and add-to-cart event types translated to one or more FB event(s).

##### To deploy
Run commands from within the specific cloud functions folder.

So for deploying `purchase_addtocart` cloud function the command will be 

```shell
gcloud --project canada-goose-fb-capi functions deploy purchase_addtocart --entry-point main \
--runtime python38 --trigger-http --allow-unauthenticated --timeout 60
```