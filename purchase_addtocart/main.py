import logging
import os
from typing import Type
from dateutil import parser
from facebook_business.api import FacebookAdsApi
from facebook_business.adobjects.serverside import (
    user_data,
    custom_data,
    event,
    event_request,
    action_source
)
FB_ACCESS_TOKEN = os.environ.get("FB_ACCESS_TOKEN",
                                 "EAACZBQdYXFngBAP1GAw0ldwMlGJCM5XuSJdCzuFuE8MO8iwjcSDN"
                                 "mgRNW73jpq5k9q5PmpsdmVClBRI3Jy7ugomFUuc8AUxwbu0kolrd5j"
                                 "KY49cpWFroE4ZAVZAoJuO72AyTNbkNX9VGnBpQU5Ap5Xmrcdw7njAS"
                                 "RUcp4R3cVLeD9R5FtkI")
FB_PIXEL_ID = os.environ.get("FB_PIXEL_ID", "2803479459974066")
FB_TEST_CODE = os.environ.get("FB_TEST_CODE")
logging.basicConfig(
    format="%(asctime)s %(levelname)s: %(message)s",
    level=os.environ.get("LOG_LEVEL", "INFO"),
)

FacebookAdsApi.init(access_token=FB_ACCESS_TOKEN, debug=True)


def validate_fb_event(event_obj: event.Event) -> None:
    """
    Checks the the FB event object is valid.
    - Has at least one user data parameter defined.
    - Can be normalised successfully
    :param event_obj:
    :raises Exception: If invalid
    :return: None
    """
    logging.info(f"VALIDATING {event_obj.event_id}")
    normalised_event = event_obj.normalize()  # exception raised if unsuccessful
    if not normalised_event["user_data"]:
        # user_data is an empty dict so raise exception
        raise ValueError("Event does not have any user data")

def standardize_price(value_string):
    """
    Some price parameter values have both periods and commas in them e.g "595,00" and 1,595.00"
    as seen in logs
    Function takes in a string and returns a string with a period at the end
    :param value_string:
    :return: string
    """
    if value_string and type(value_string) is not float and type(value_string) is not int:
        seperators = [".",","]
        value_string = list(value_string)
        for item in value_string:
            if item in seperators:
                value_string.remove(item)
        value_string.insert(-2, ".")
        value_string = "".join(value_string)

    return value_string

def send_event_to_fb(payload, headers):
    """
    Extract data from payload, create FB and send appropriate FB event object
    Ensures the sanity of the object sent to FB!
    :param payload:
    :param headers:
    :return:
    """

    logging.info(str(payload["type"]).upper() +
        " OBJECT RECEIVED. EXTRACTING REQUIRED FIELDS FOR FB EVENT")
    logging.info(payload)


    if "total_value" not in payload:
        logging.info("NO TOTAL VALUE DATA IN PAYLOAD, SKIPPING....")
        return

    if (type(payload["total_value"]) != int) and (type(payload["total_value"]) != str) and (type(payload["total_value"]) != float):
        var_type = (str(type(payload["total_value"])).split(" "))[1][0:-1:]
        logging.info(f"INVALID VALUE PARAMETER {var_type.upper()} RECEIVED, SKIPPING")
        return


    if "token" in payload:
        user_id = payload["token"]
    else:
        user_id = payload["order_id"]

    user_data_obj = user_data.UserData(
        email = payload.get("email"),
        phone = payload.get("phone"),
        date_of_birth = payload.get("date_of_birth"),
        last_name = payload.get("last_name"),
        first_name = payload.get("first_name"),
        city = payload.get("city"),
        state = payload.get("state"),
        country_code = payload.get("country_code"),
        zip_code = payload.get("post_code"),
        client_ip_address = payload.get("client_ip_address"),
        client_user_agent = payload.get("client_user_agent"),
        subscription_id = user_id

    )

    custom_data_obj = custom_data.CustomData(
        value=float(standardize_price(payload["total_value"])),
        currency=payload["currency"],
        content_ids = payload.get("content_ids")
    )

    if "gender" in payload:
        if payload.get("gender"):
            user_data_obj.gender = payload["gender"]

    event_action_source = action_source.ActionSource.OTHER
    if payload.get("client_ip_address") and payload.get("client_user_agent"):
        event_action_source = action_source.ActionSource.WEBSITE

    fb_event = event.Event(event_name=payload["type"],
                           event_time= int(payload["event_time"]),
                           event_source_url=payload["event_source_url"],
                           event_id= user_id,
                           user_data=user_data_obj,
                           custom_data=custom_data_obj,
                           action_source=event_action_source
                           )

    try:
        validate_fb_event(fb_event)
        logging.info(f"FB event created: {fb_event.to_dict()}")
        fb_event_request = event_request.EventRequest(
            events=[fb_event],
            pixel_id=FB_PIXEL_ID,
        )

        if FB_TEST_CODE:
            fb_event_request.test_event_code = FB_TEST_CODE
            logging.debug("SENDING AS TEST EVENT TO FB")
        else:
            logging.debug("SENDING AS REAL EVENT TO FB")

        response = fb_event_request.execute()
        logging.info(response.to_dict())
    except ValueError as exc:
        logging.info(f"{repr(exc)} SKIPPING")


def main(request):
    """
    Responds to any HTTP request.
    Args:
        request (flask.Request): HTTP request object.
    Returns:
        The response text or any set of values that can be turned into a
        Response object using
        # flask.Flask.make_response>`.
        `make_response <http://flask.pocoo.org/docs/1.0/api/
    """

    if request.method == 'POST':
        send_event_to_fb(payload=request.get_json(), headers=request.headers)
        return "OK", 200, {'Access-Control-Allow-Origin': '*', }
    else:
        return "unsupported method", 400, {'Access-Control-Allow-Origin': '*', }
